package pe.edu.cibertec.cnx;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	
	private static EntityManagerFactory FACTORY=Persistence.createEntityManagerFactory("BD");
		
	//otbtiene la conexion de la base de datos
	
	public static EntityManager  getEntityManager(){
		return FACTORY.createEntityManager();
	}

	
}
