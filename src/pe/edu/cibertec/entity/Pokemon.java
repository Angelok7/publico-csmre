package pe.edu.cibertec.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_pokemon")
public class Pokemon {
	

	@Id
	@GeneratedValue
	private long id;
	
	@Column(name="nombre",nullable=false, length=50)
	private String nombre;
	
	@Column(name="vida",nullable=false)
	private int vida;
	
	@Column(name="ataque",nullable=false)
	private int ataque;
	
	@Column(name="tipo",nullable=false, length=50)
	private String tipo;
	
	@Column(name="fecha_captura")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_captura;


	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getVida() {
		return vida;
	}


	public void setVida(int vida) {
		this.vida = vida;
	}


	public int getAtaque() {
		return ataque;
	}


	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public Date getFecha_captura() {
		return fecha_captura;
	}


	public void setFecha_captura(Date fecha_captura) {
		this.fecha_captura = fecha_captura;
	}
	

	
	
}
